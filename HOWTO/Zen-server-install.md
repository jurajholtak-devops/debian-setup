#monitoring

sudo ipmitool sdr

--show discard enabled
lsblk -D


```
# Save GPT disks
sgdisk --backup=/partitions-backup-$(basename $source).sgdisk $source
sgdisk --backup=/partitions-backup-$(basename $dest).sgdisk $dest

# Copy $source layout to $dest and regenerate GUIDs
sgdisk --replicate=$dest $source
sgdisk -G $dest

sfdisk -d $source | sfdisk $dest

```

```
label: gpt
label-id: 5A937181-5559-49E5-9A10-139587D77937
device: /dev/nvme0n1
unit: sectors
first-lba: 34
last-lba: 4000797326

/dev/nvme0n1p1 : start=        2048, size=    39061504, type=C12A7328-F81F-11D2-BA4B-00A0C93EC93B, uuid=098C310D-F590-4FD7-865F-5E8510EE8098
/dev/nvme0n1p2 : start=    39063552, size=    19531776, type=A19D880F-05FC-4D3B-A006-743F0F84911E, uuid=39B7ED67-64F2-4125-B4E1-10C6BE581047
/dev/nvme0n1p3 : start=    58595328, size=   195311616, type=A19D880F-05FC-4D3B-A006-743F0F84911E, uuid=8D175E59-2ACC-40FE-B3ED-AAB5B6A96EEA
/dev/nvme0n1p4 : start=   253906944, size=   234375168, type=0FC63DAF-8483-4772-8E79-3D69D8477DE4, uuid=EF976A2C-B584-437A-9AEC-C85CBD3C4058
```


GPT fdisk (gdisk) version 1.0.3

Partition table scan:
  MBR: protective
  BSD: not present
  APM: not present
  GPT: present

Found valid GPT with protective MBR; using GPT.

Command (? for help): p
Disk /dev/nvme0n1: 4000797360 sectors, 1.9 TiB
Model: ADATA SX8200PNP                         
Sector size (logical/physical): 512/512 bytes
Disk identifier (GUID): EEB734FE-D91C-4B46-957C-179FDC2E6328
Partition table holds up to 128 entries
Main partition table begins at sector 2 and ends at sector 33
First usable sector is 34, last usable sector is 4000797326
Partitions will be aligned on 2048-sector boundaries
Total free space is 3407 sectors (1.7 MiB)

Number  Start (sector)    End (sector)  Size       Code  Name
   1            2048        83888127   40.0 GiB    EF00  efi_raid1
   2        83888128       136316927   25.0 GiB    FD00  swap_raid1
   3       136316928       404752383   128.0 GiB   FD00  system_ext4_raid1
   4       404752384       673187839   128.0 GiB   8300  spare_ext4_raid1
  30       673187840      3750748814   1.4 TiB     FD00  nvme_n30_raid1
  99      3750750208      4000797326   119.2 GiB   8300  discarded_space


linuxadmin@zen2:~$ sudo gdisk /dev/sda
GPT fdisk (gdisk) version 1.0.3

Partition table scan:
  MBR: protective
  BSD: not present
  APM: not present
  GPT: present

Found valid GPT with protective MBR; using GPT.

Command (? for help): p
Disk /dev/sda: 19532873728 sectors, 9.1 TiB
Model: ST10000NM0086-2A
Sector size (logical/physical): 512/4096 bytes
Disk identifier (GUID): 00E3DA00-247C-4EC1-807D-2B4CA5DE5BFE
Partition table holds up to 128 entries
Main partition table begins at sector 2 and ends at sector 33
First usable sector is 34, last usable sector is 19532873694
Partitions will be aligned on 2048-sector boundaries
Total free space is 2014 sectors (1007.0 KiB)

Number  Start (sector)    End (sector)  Size       Code  Name
   1            2048        83888127   40.0 GiB    EF00  efi_raid1
   2        83888128       136316927   25.0 GiB    FD00  swap_raid1
   3       136316928       404752383   128.0 GiB   FD00  system_ext4_raid1
   4       404752384       673187839   128.0 GiB   8300  spare_ext4_raid1
  10       673187840      3894413311   1.5 TiB     FD00  storage_n10_raid0
  20      3894413312     19532873694   7.3 TiB     FD00  storage_n20_raid1


efibootmgr -c -d /dev/nvme0n1 -p 1 -L "boot server (nvme0)" -l "\\EFI\\debian\\shimx64.efi"


   50  cat /proc/mdstat 
   51  sudo mount /dev/md/zen1\:system /mnt/
   52  sudo mount /dev/md/zen1\:efi /mnt/boot/efi/
   54  sudo mount --bind /dev/ /mnt/dev/
   55  sudo mount --bind /proc/ /mnt/proc/
   56  sudo mount --bind /sys/ /mnt/sys/
   69  sudo mount --bind /sys/firmware/efi/efivars/ /mnt/sys/firmware/efi/efivars/
   70  sudo chroot /mnt/
   71  history

sudo mdadm --create --verbose --metadata=1.0 --name=zen1:efi_raid1 /dev/md/efi_raid1 --level=mirror --bitmap=internal --raid-devices=4 /dev/nvme0n1p1 /dev/nvme1n1p1 --write-mostly /dev/sda1 /dev/sdb1
sudo mdadm --create --verbose --metadata=1.2 --name=zen1:swap_raid1 /dev/md/swap_raid1 --level=mirror --bitmap=internal --raid-devices=4 /dev/nvme0n1p2 /dev/nvme1n1p2 --write-mostly /dev/sda2 /dev/sdb2
sudo mdadm --create --verbose --metadata=1.2 --name=zen1:system_ext4_raid1 /dev/md/system_ext4_raid1 --level=mirror --bitmap=internal --raid-devices=4 /dev/nvme0n1p3 /dev/nvme1n1p3 --write-mostly /dev/sda3 /dev/sdb3
sudo mdadm --create --verbose --metadata=1.2 --name=zen1:nvme_n30_raid1 /dev/md/nvme_n30_raid1 --level=mirror --bitmap=internal --raid-devices=2 /dev/nvme0n1p30 /dev/nvme1n1p30
sudo mdadm --create --verbose --metadata=1.2 --name=zen1:storage_n10_raid0 /dev/md/storage_n10_raid1 --level=0 --chunk=65536 --raid-devices=2 /dev/sda10 /dev/sdb10
sudo mdadm --create --verbose --metadata=1.2 --name=zen1:storage_n20_raid1 /dev/md/storage_n20_raid1 --level=mirror --bitmap=internal --raid-devices=2 /dev/sda20 /dev/sdb20

sudo pvcreate /dev/md/nvme_n30_raid1
sudo pvcreate /dev/md/storage_n10_raid0
sudo pvcreate /dev/md/storage_n20_raid1

sudo vgcreate vg_low_slow_safe /dev/md/storage_n20_raid1
sudo vgcreate vg_low_slow_unsafe /dev/md/storage_n10_raid0
sudo vgcreate vg_low_fast_safe /dev/md/nvme_n30_raid1

sudo lvcreate --autobackup y --wipesignatures y --zero y -n lv_low_slow_test_backend -L 128G vg_low_slow_safe
sudo lvcreate --autobackup y --wipesignatures y --zero y -n lv_low_system_backend -L 3050G vg_low_slow_safe
sudo lvcreate --autobackup y --wipesignatures y --zero y -n lv_low_data_backend -l 100%FREE vg_low_slow_safe
sudo lvcreate --autobackup y --wipesignatures y --zero y -n lv_low_slow_unsafe_backend -l 100%FREE vg_low_slow_unsafe
sudo lvcreate --autobackup y --wipesignatures y --zero y -n lv_low_fast_test_cache -L 10G vg_low_fast_safe
sudo lvcreate --autobackup y --wipesignatures y --zero y -n lv_low_fast_readcache -L 700G vg_low_fast_safe
sudo lvcreate --autobackup y --wipesignatures y --zero y -n lv_low_fast_writecache -l 100%FREE vg_low_fast_safe



make-bcache -B -b $((1024*128*4)) -w 4096 -o 8192 /dev/vg_low_slow_safe/lv_low_system_backend
make-bcache -C -b $((1024*1024*4)) -w 512 -o 8192 /dev/vg_low_fast_safe/lv_low_fast_writecache --discard --writeback
echo uuid > /sys/block/bcache0/bcache/attach
echo bcache_cluster_writeback >  /sys/block/bcache0/bcache/label
echo writeback > /sys/block/bcache0/bcache/cache_mode

root@zen2:/home/linuxadmin# cat /etc/rc.local 
#!/bin/sh -e
# This script is executed at the end of each multiuser runlevel

echo 0 > /sys/block/bcache0/bcache/sequential_cutoff
echo 0 > /sys/block/bcache0/bcache/cache/congested_write_threshold_us
echo 0 > /sys/block/bcache0/bcache/cache/congested_read_threshold_us
echo 40 > /sys/block/bcache0/bcache/writeback_percent

echo 0 > /sys/block/bcache1/bcache/sequential_cutoff
echo 0 > /sys/block/bcache1/bcache/cache/congested_write_threshold_us
echo 0 > /sys/block/bcache1/bcache/cache/congested_read_threshold_us
echo 40 > /sys/block/bcache1/bcache/writeback_percent

echo 0 > /sys/block/bcache2/bcache/sequential_cutoff
echo 0 > /sys/block/bcache2/bcache/cache/congested_write_threshold_us
echo 0 > /sys/block/bcache2/bcache/cache/congested_read_threshold_us
echo 40 > /sys/block/bcache2/bcache/writeback_percent

echo 0 > /sys/block/bcache3/bcache/sequential_cutoff
echo 0 > /sys/block/bcache3/bcache/cache/congested_write_threshold_us
echo 0 > /sys/block/bcache3/bcache/cache/congested_read_threshold_us
echo 40 > /sys/block/bcache3/bcache/writeback_percent

exit 0
###################################################################

chmod 755 /etc/rc.local

sync

pvcreate --dataalignment 4m /dev/bcache0
vgcreate vg_cluster_writeback /dev/bcache0

pvcreate --dataalignment 4m /dev/bcache1
vgcreate vg_cluster_data /dev/bcache1



515  make-bcache -B -b 524288 -w 4096 -o 8192 /dev/vg_hdd_g1_raid1/lv_hdd_g1_writeback_backend
  516  make-bcache -C -b 4194304 -w 4096 -o 8192 /dev/vg_nvme_g1_raid1/lv_nvme_g1_writeback_cache --discard --writeback
  517  echo fd95e128-8031-4f98-ab18-a4ba7a6e0980 > /sys/block/bcache0/bcache/attach 
  518  cat /sys/block/bcache0/bcache/cache_mode 
  519  echo writeback > /sys/block/bcache0/bcache/cache_mode 
  520  echo 0 > /sys/block/bcache0/bcache/sequential_cutoff
  521  echo 0 > /sys/block/bcache0/bcache/cache/congested_write_threshold_us
  522  echo 0 > /sys/block/bcache0/bcache/cache/congested_read_threshold_us
  523  echo 40 > /sys/block/bcache0/bcache/writeback_percent
  524  pvcreate --dataalignment 4m /dev/bcache0
  525  vgcreate vg_cluster_writeback /dev/bcache0






sudo gnt-cluster init --vg-name vg_cluster_writeback --master-netdev br-main --master-netmask 24 --enabled-hypervisors kvm --nic-parameters link=br-main,mode=openvswitch -B vcpus=2,memory=1G -s 10.234.0.51 cluster1.proaut.lan

#DRBD
sudo gnt-cluster modify -D drbd:dynamic-resync=True,c-plan-ahead=20,resync-rate=950000,c-max-rate=950000,c-min-rate=100000,c-fill-target=1000000,net-custom="--max-buffers 32k --sndbuf-size 1M --rcvbuf-size 2M --verify-alg crc32c --data-integrity-alg crc32c --protocol C"

#Live migration
sudo gnt-cluster modify -H kvm:migration_bandwidth=950


Performance:

#Windows
sudo gnt-instance modify -H disk_aio=native,kernel_path=,disk_cache=none,disk_discard=unmap,disk_type=paravirtual,cpu_type="host\,hv_relaxed\,hv_spinlocks\=0x1fff\,hv_vapic\,hv_time\,hv_synic\,hv_stimer\,hv-vpindex",usb_mouse=tablet,nic_type=paravirtual,vhost_net=true,cpu_cores=4,cpu_threads=1,cpu_sockets=1 -B vcpus=4 wintest1

#paravirtual scsi (because of ssd trim)
gnt-cluster modify -H kvm:scsi_controller_type=virtio-scsi-pci,disk_type=scsi-hd

#Spice
sudo gnt-instance modify -H spice_image_compression=auto_glz,spice_jpeg_wan_compression=auto,spice_playback_compression=true,spice_streaming_video=filter,spice_zlib_glz_wan_compression=auto wintest1
sudo gnt-instance modify -H vga=none,kvm_extra="-pflash /usr/share/qemu/OVMF.fd\,readonly -device qxl-vga\,vgamem_mb=32\,heads=2 -device nec-usb-xhci\,id=usb -chardev spicevmc\,name=usbredir\,id=usbredirchardev1 -device usb-redir\,chardev=usbredirchardev1\,id=usbredirdev1" wintest1

#2 fucking monitors

sudo gnt-instance modify -H vga=none,kvm_extra="-device qxl-vga\,id=bla.0\,ram_size_mb=256\,vgamem_mb=32 -device qxl\,ram_size_mb=256\,vgamem_mb=32 -device nec-usb-xhci\,id=usb -chardev spicevmc\,name=usbredir\,id=usbredirchardev1 -device usb-redir\,chardev=usbredirchardev1\,id=usbredirdev1 -device virtserialport\,bus=spice.0\,nr=2\,chardev=charchannel1\,id=channel1\,name=org.spice-space.stream.0 -chardev spiceport\,name=org.spice-space.stream.0\,id=charchannel1" wintest1
$cm,dl;ine
qemu-system-x86_64 -enable-kvm -name wintest1.proaut.lan -m 8192 -smp 4,cores=4,threads=1,sockets=1 -pidfile /var/run/ganeti/kvm-hypervisor/pid/wintest1.proaut.lan -soundhw hda -device virtio-balloon -daemonize -machine pc -monitor unix:/var/run/ganeti/kvm-hypervisor/ctrl/wintest1.proaut.lan.monitor,server,nowait -serial unix:/var/run/ganeti/kvm-hypervisor/ctrl/wintest1.proaut.lan.serial,server,nowait -usb -usbdevice tablet -device virtio-serial-pci,id=spice -device virtserialport,chardev=spicechannel0,name=com.redhat.spice.0 -chardev spicevmc,id=spicechannel0,name=vdagent -spice addr=0.0.0.0,port=11001,disable-ticketing,image-compression=auto_glz,jpeg-wan-compression=auto,zlib-glz-wan-compression=auto,streaming-video=filter -cpu host,hv_relaxed,hv_spinlocks=0x1fff,hv_vapic,hv_time,hv_synic,hv_stimer,hv-vpindex -vga none -uuid e2737d32-a11a-4111-81e6-a19b5cdad9fd -device qxl-vga,id=bla.0,ram_size_mb=256,vgamem_mb=32 -device qxl,ram_size_mb=256,vgamem_mb=32 -device nec-usb-xhci,id=usb -chardev spicevmc,name=usbredir,id=usbredirchardev1 -device usb-redir,chardev=usbredirchardev1,id=usbredirdev1 -netdev type=tap,id=nic-7e250cb2-fc5b-4a2e,fd=9,vhostfd=10,vhost=on -device virtio-net-pci,bus=pci.0,id=nic-7e250cb2-fc5b-4a2e,addr=0xd,netdev=nic-7e250cb2-fc5b-4a2e,mac=aa:00:00:01:e1:26 -qmp unix:/var/run/ganeti/kvm-hypervisor/ctrl/wintest1.proaut.lan.qmp,server,nowait -qmp unix:/var/run/ganeti/kvm-hypervisor/ctrl/wintest1.proaut.lan.kvmd,server,nowait -boot c -device virtio-blk-pci,bus=pci.0,id=disk-287ee75d-d607-4f74,addr=0xc,drive=disk-287ee75d-d607-4f74 -drive file=/var/run/ganeti/instance-disks/wintest1.proaut.lan:0,format=raw,if=none,cache=none,aio=native,discard=unmap,id=disk-287ee75d-d607-4f74 -S



https://wiki.archlinux.org/index.php/QEMU#Multi-monitor_support

https://www.ibm.com/support/knowledgecenter/en/linuxonibm/liaag/wkvm/wkvm_c_tune_kvm.htm
echo 80000 > /sys/module/kvm/parameters/halt_poll_ns 

OpenVSwitch:
ovs-vsctl set Open_vSwitch . other_config:hw-offload=true
ovs-vsctl set Open_vSwitch . other_config:tc-policy=skip_sw



sudo gnt-cluster modify -H kvm:cdrom_disk_type=ide,disk_aio=native,disk_cache=none,disk_discard=unmap,disk_type=paravirtual,initrd_path=,kernel_path=
sudo gnt-cluster modify -H kvm:cpu_type=host\\,hv_relaxed\\,hv_spinlocks=0x1fff\\,hv_vapic\\,hv_time\\,hv_synic\\,hv_stimer\\,hv-vpindex
#windows
sudo gnt-cluster modify -H kvm:vga=none,kvm_extra="-pflash /usr/share/qemu/OVMF.fd -global PIIX4_PM.disable_s3=1 -global PIIX4_PM.disable_s4=1 -device qxl-vga\,id=bla.0\,ram_size_mb=256\,vgamem_mb=32 -device qxl\,ram_size_mb=256\,vgamem_mb=32 -device virtserialport\,bus=spice.0\,nr=2\,chardev=charchannel1\,id=channel1\,name=org.spice-space.stream.0 -chardev spiceport\,name=org.spice-space.stream.0\,id=charchannel1 -device nec-usb-xhci\,id=usb -chardev spicevmc\,name=usbredir\,id=usbredirchardev1 -device usb-redir\,chardev=usbredirchardev1\,id=usbredirdev1 -chardev spicevmc\,name=usbredir\,id=usbredirchardev2 -device usb-redir\,chardev=usbredirchardev2\,id=usbredirdev2 -chardev spicevmc\,name=usbredir\,id=usbredirchardev3 -device usb-redir\,chardev=usbredirchardev3\,id=usbredirdev3"
#linux
sudo gnt-cluster modify -H kvm:vga=vga,kvm_extra="-pflash /usr/share/qemu/OVMF.fd -global PIIX4_PM.disable_s3=1 -global PIIX4_PM.disable_s4=1 -device nec-usb-xhci\,id=usb -chardev spicevmc\,name=usbredir\,id=usbredirchardev1 -device usb-redir\,chardev=usbredirchardev1\,id=usbredirdev1 -chardev spicevmc\,name=usbredir\,id=usbredirchardev2 -device usb-redir\,chardev=usbredirchardev2\,id=usbredirdev2 -chardev spicevmc\,name=usbredir\,id=usbredirchardev3 -device usb-redir\,chardev=usbredirchardev3\,id=usbredirdev3"
# and we use
sudo gnt-cluster modify -H kvm:vga=qxl,kvm_extra="-global PIIX4_PM.disable_s3=1 -global PIIX4_PM.disable_s4=1 -device virtserialport\,bus=spice.0\,nr=2\,chardev=charchannel1\,id=channel1\,name=org.spice-space.stream.0 -chardev spiceport\,name=org.spice-space.stream.0\,id=charchannel1 -device nec-usb-xhci\,id=usb -chardev spicevmc\,name=usbredir\,id=usbredirchardev1 -device usb-redir\,chardev=usbredirchardev1\,id=usbredirdev1 -chardev spicevmc\,name=usbredir\,id=usbredirchardev2 -device usb-redir\,chardev=usbredirchardev2\,id=usbredirdev2 -chardev spicevmc\,name=usbredir\,id=usbredirchardev3 -device usb-redir\,chardev=usbredirchardev3\,id=usbredirdev3"

sudo gnt-cluster modify -H kvm:spice_password_file=/etc/spice-password 
sudo gnt-cluster modify -H kvm:spice_bind=0.0.0.0,spice_image_compression=auto_glz,spice_jpeg_wan_compression=auto,spice_playback_compression=true,spice_streaming_video=filter,vga=qxl,usb_mouse=tablet
sudo gnt-cluster modify -B maxmem=4096,minmem=1024,vcpus=3
sudo gnt-cluster modify -H kvm:cpu_sockets=1,cpu_threads=1
sudo gnt-cluster modify -H kvm:vhost_net=true
sudo gnt-cluster modify -H kvm:soundhw=hda
sudo gnt-cluster modify -H kvm:use_chroot=true
sudo gnt-cluster modify -H kvm:kvm_flag=enabled
sudo useradd -s /usr/sbin/nologin -r -M -d /dev/null gnt-jail
sudo gnt-cluster modify -H kvm:security_model=user,security_domain=gnt-jail

#max out cluster limits
sudo gnt-cluster modify --ipolicy-bounds-specs max:cpu-count=12,disk-count=16,disk-size=$((4*1024*1024)),memory-size=65536,nic-count=8,spindle-use=12/min:cpu-count=1,disk-count=1,disk-size=1024,memory-size=128,nic-count=1,spindle-use=1




sudo gnt-instance start -H machine_version=pc,boot_order=cdrom,cdrom_image_path=/home/linuxadmin/windows.iso,cdrom2_image_path=/home/linuxadmin/virtio-win.iso wintest1
#hinf EFI Shell
# fs0:

#change windows10 from HDD to SSD
as Admin
run: WinSAT formal -v


sudo gnt-instance add -t plain -s 100G -o noop --no-start wintest1.proaut.lan


lvcreate -l100%FREE -s -n wintest1_system_data_snap-`date +%Y-%m-%d_%H-%M-%S` /dev/vg_cluster_writeback/4aacbf49-0e0c-4dfc-981c-3e3e764c702d.disk0_data

sudo gnt-network add --network=10.232.1.0/24 --gateway=10.232.1.1 --mac-prefix=0A:E8:01 proaut_lan
sudo gnt-group add zen_servers
sudo gnt-group add --node ovs=false,ovs_name=br-proaut_lan,ovs_link=eno2 proaut_lan_grp 
sudo gnt-group assign-nodes proaut_lan_grp zen1 zen2
sudo gnt-network connect -N mode=openvswitch,link=br-proaut_lan proaut_lan proaut_lan_grp



#Powershell to download RSAT
Get-WindowsCapability -Name RSAT* -Online | Add-WindowsCapability -Online
# then update help
Update-Help
# confirm that RSAT is installed
Get-WindowsCapability -Name RSAT* -Online | Select-Object -Property DisplayName, State
