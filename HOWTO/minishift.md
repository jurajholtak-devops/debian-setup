minishift delete --force --clear-cache

rm -rf ~/.kube
rm -rf ~/.helm/
rm -rf ~/.minishift/


minishift addons install --defaults

minishift addons enable admin-user

minishift start --profile minishift --vm-driver virtualbox --memory 8GB --cpus=4 --disk-size=50GB

#############
Minishift root access

login: docker
pass: tcuser

switch to root:
sudo -s
#############



# surprise! there is no true superuser by default you have to apply it as an add-on. 
minishift addon apply admin-user
# note as the superuse to install tiller into its own namespace
oc login -u admin -p admin
# important! OCD demo scripts expect to `export TILLER_NAMESPACE=tiller-namespace` matches this project:
oc new-project tiller-namespace
# create a system account for tiller
oc create sa tiller
# in a real cluster I wouldn't grant Tiller cluster admin I would grant it edit on specific projects
oc adm policy add-cluster-role-to-user cluster-admin -z tiller
# the default developer user must be able to "see" teller from outside of minishift
oc policy add-role-to-user view developer -n tiller-namespace
oc create role podreader --verb=get,list,watch --resource=pod -n tiller-namespace
oc adm policy add-role-to-user podreader developer --role-namespace=tiller-namespace -n tiller-namespace
# our regular developer user must be able to communicate with tiller from outside of minishift
oc create role portforward --verb=create,get,list,watch --resource=pods/portforward -n tiller-namespace
oc adm policy add-role-to-user portforward developer --role-namespace=tiller-namespace -n tiller-namespace
# our regular develpoer should see that the roles are installed when running the wizard to not get warnings
oc create role rolereader --verb=get,list,watch --resource=roles -n tiller-namespace
oc adm policy add-role-to-user rolereader developer --role-namespace=tiller-namespace -n tiller-namespace
# install tiller
helm init --service-account tiller --tiller-namespace tiller-namespace
# Note: you will need to export this env var in every shell you wish to use 'helm list' etc
export TILLER_NAMESPACE=tiller-namespace

export HELM_HOST="$(minishift ip):$(oc get svc/tiller -o jsonpath='{.spec.ports[0].nodePort}' -n tiller-namespace --as=system:admin)"
oc expose deployment/tiller-deploy --target-port=tiller --type=NodePort --name=tiller -n tiller-namespace





