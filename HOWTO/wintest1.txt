- Instance name: wintest1.proaut.lan
  UUID: e2737d32-a11a-4111-81e6-a19b5cdad9fd
  Serial number: 302
  Creation time: 2019-11-12 16:30:15
  Modification time: 2019-12-20 21:02:40
  State: configured to be down, actual state is down
  Nodes: 
    - primary: zen1.proaut.lan
      group: default (UUID 4bb86827-9e63-47d5-aa7f-65058326e9df)
    - secondaries: 
  Operating system: noop
  Operating system parameters: 
  Allocated network port: 11001
  Hypervisor: kvm
  Hypervisor parameters: 
    acpi: default (True)
    boot_order: default (disk)
    cdrom2_image_path: default ()
    cdrom_disk_type: ide
    cdrom_image_path: default ()
    cpu_cores: 4
    cpu_mask: default (all)
    cpu_sockets: 1
    cpu_threads: 1
    cpu_type: host,hv_relaxed,hv_spinlocks=0x1fff,hv_vapic,hv_time,hv_synic,hv_stimer,hv-vpindex
    disk_aio: native
    disk_cache: none
    disk_discard: unmap
    disk_type: paravirtual
    floppy_image_path: default ()
    initrd_path: default ()
    kernel_args: default (ro)
    kernel_path: 
    keymap: default ()
    kvm_extra: -device qxl-vga,id=bla.0,ram_size_mb=256,vgamem_mb=32 -device qxl,ram_size_mb=256,vgamem_mb=32 -device nec-usb-xhci,id=usb -chardev spicevmc,name=usbredir,id=usbredirchardev1 -device usb-redir,chardev=usbredirchardev1,id=usbredirdev1 -device virtserialport,bus=spice.0,nr=2,chardev=charchannel1,id=channel1,name=org.spice-space.stream.0 -chardev spiceport,name=org.spice-space.stream.0,id=charchannel1
    kvm_flag: default ()
    kvm_path: default (/usr/bin/kvm)
    kvm_pci_reservations: default (12)
    machine_version: pc
    mem_path: default ()
    migration_caps: default ()
    migration_downtime: default (30)
    nic_type: paravirtual
    reboot_behavior: default (reboot)
    root_path: default (/dev/vda1)
    scsi_controller_type: default (lsi)
    security_domain: default ()
    security_model: default (none)
    serial_console: default (True)
    serial_speed: default (38400)
    soundhw: hda
    spice_bind: default (0.0.0.0)
    spice_image_compression: auto_glz
    spice_ip_version: default (0)
    spice_jpeg_wan_compression: auto
    spice_password_file: default ()
    spice_playback_compression: True
    spice_streaming_video: filter
    spice_tls_ciphers: default (HIGH:-DES:-3DES:-EXPORT:-DH)
    spice_use_tls: default (False)
    spice_use_vdagent: default (True)
    spice_zlib_glz_wan_compression: auto
    usb_devices: default ()
    usb_mouse: tablet
    use_chroot: default (False)
    use_guest_agent: False
    use_localtime: default (False)
    user_shutdown: default (False)
    vga: none
    vhost_net: True
    virtio_net_queues: default (1)
    vnc_bind_address: default ()
    vnc_password_file: default ()
    vnc_tls: default (False)
    vnc_x509_path: default ()
    vnc_x509_verify: default (False)
    vnet_hdr: default (True)
  Back-end parameters: 
    always_failover: default (False)
    auto_balance: default (True)
    maxmem: 8192
    memory: default (8192)
    minmem: 8192
    spindle_use: default (1)
    vcpus: 4
  NICs: 
    - nic/0: 
      MAC: aa:00:00:01:e1:26
      IP: None
      mode: openvswitch
      link: br-main
      vlan: 
      network: None
      UUID: 7e250cb2-fc5b-4a2e-95fa-d3ce19269943
      name: None
  Disk template: plain
  Disks: 
    - disk/0: plain, size 80.0G
      access mode: rw
      logical_id: vg_cluster_writeback/918c6787-022a-4b32-963f-e82c0cad92ac.disk0
      name: None
      UUID: 287ee75d-d607-4f74-8376-3d383be6639b
