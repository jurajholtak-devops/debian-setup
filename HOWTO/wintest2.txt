- Instance name: wintest2.proaut.lan
  UUID: d2b0c693-6ed7-46d9-9fd5-a139533bb7bf
  Serial number: 80
  Creation time: 2019-11-25 15:58:05
  Modification time: 2019-12-22 21:57:36
  State: configured to be down, actual state is down
  Nodes: 
    - primary: zen1.proaut.lan
      group: default (UUID 4bb86827-9e63-47d5-aa7f-65058326e9df)
    - secondaries: zen2.proaut.lan (group default, group UUID 4bb86827-9e63-47d5-aa7f-65058326e9df)
  Operating system: noop
  Operating system parameters: 
  Allocated network port: 11002
  Hypervisor: kvm
  Hypervisor parameters: 
    acpi: default (True)
    boot_order: default (disk)
    cdrom2_image_path: default ()
    cdrom_disk_type: ide
    cdrom_image_path: default ()
    cpu_cores: 4
    cpu_mask: default (all)
    cpu_sockets: 1
    cpu_threads: 1
    cpu_type: host,hv_relaxed,hv_spinlocks=0x1fff,hv_vapic,hv_time,hv_synic,hv_stimer,hv-vpindex
    disk_aio: native
    disk_cache: none
    disk_discard: unmap
    disk_type: paravirtual
    floppy_image_path: default ()
    initrd_path: default ()
    kernel_args: default (ro)
    kernel_path: 
    keymap: default ()
    kvm_extra: -global PIIX4_PM.disable_s3=1 -global PIIX4_PM.disable_s4=1 -pflash /usr/share/qemu/OVMF.fd -device qxl-vga,vgamem_mb=32 -device nec-usb-xhci,id=usb -chardev spicevmc,name=usbredir,id=usbredirchardev1 -device usb-redir,chardev=usbredirchardev1,id=usbredirdev1
    kvm_flag: default ()
    kvm_path: default (/usr/bin/kvm)
    kvm_pci_reservations: default (12)
    machine_version: default ()
    mem_path: default ()
    migration_caps: default ()
    migration_downtime: default (30)
    nic_type: paravirtual
    reboot_behavior: default (reboot)
    root_path: default (/dev/vda1)
    scsi_controller_type: default (lsi)
    security_domain: default ()
    security_model: default (none)
    serial_console: default (True)
    serial_speed: default (38400)
    soundhw: hda
    spice_bind: default (0.0.0.0)
    spice_image_compression: auto_glz
    spice_ip_version: default (0)
    spice_jpeg_wan_compression: auto
    spice_password_file: default ()
    spice_playback_compression: True
    spice_streaming_video: filter
    spice_tls_ciphers: default (HIGH:-DES:-3DES:-EXPORT:-DH)
    spice_use_tls: default (False)
    spice_use_vdagent: default (True)
    spice_zlib_glz_wan_compression: auto
    usb_devices: default ()
    usb_mouse: tablet
    use_chroot: default (False)
    use_guest_agent: default (False)
    use_localtime: default (False)
    user_shutdown: default (False)
    vga: none
    vhost_net: True
    virtio_net_queues: default (1)
    vnc_bind_address: default ()
    vnc_password_file: default ()
    vnc_tls: default (False)
    vnc_x509_path: default ()
    vnc_x509_verify: default (False)
    vnet_hdr: default (True)
  Back-end parameters: 
    always_failover: default (False)
    auto_balance: default (True)
    maxmem: 8192
    memory: default (8192)
    minmem: 1024
    spindle_use: default (1)
    vcpus: 4
  NICs: 
    - nic/0: 
      MAC: aa:00:00:15:84:94
      IP: None
      mode: openvswitch
      link: br-main
      vlan: 
      network: None
      UUID: c0c3e711-49b5-4356-a298-8b197e7b4278
      name: None
  Disk template: drbd
  Disks: 
    - disk/0: drbd, size 80.0G
      access mode: rw
      nodeA: zen1.proaut.lan, minor=0
      nodeB: zen2.proaut.lan, minor=0
      port: 11003
      name: None
      UUID: afed5f38-f0ee-4722-a7b4-104e44467f1f
      child devices: 
        - child 0: plain, size 80.0G
          logical_id: vg_cluster_writeback/18246621-d9cd-42de-a4b2-ea4eccc9bb2a.disk0_data
          on secondary: /dev/vg_cluster_writeback/18246621-d9cd-42de-a4b2-ea4eccc9bb2a.disk0_data (253:7)
          name: None
          UUID: 5711d98f-97a1-4812-94ff-8dfe35681a18
        - child 1: plain, size 128M
          logical_id: vg_cluster_writeback/18246621-d9cd-42de-a4b2-ea4eccc9bb2a.disk0_meta
          on secondary: /dev/vg_cluster_writeback/18246621-d9cd-42de-a4b2-ea4eccc9bb2a.disk0_meta (253:8)
          name: None
          UUID: 9e68ba99-146b-4613-bd69-3e17e2425500
