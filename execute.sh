#!/bin/sh
git pull && ansible-playbook  --diff --connection=local --ask-become-pass -i hosts -i `hostname` site.yml --limit=`hostname`
