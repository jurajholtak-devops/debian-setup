#!/bin/sh

# manually apply this as root
/usr/sbin/usermod -aG sudo linuxadmin

apt install -y sudo ansible python3-apt tmux vim
